/*!
 * contentstack-sync
 * copyright (c) Contentstack.com
 * MIT Licensed
 */

'use strict';

/*!
 * Module dependencies
 */

let config = require('./../config')(),
    socket = require('./socket/index'),
    Sync = require('./sync');


module.exports = ( () => {
    try {
        if (typeof config === 'object') {
            if (!(config.get('contentstack.api_key') && config.get('contentstack.access_token'))) {
                throw new Error("Built.io Contentstack details are missing.");
            }
            // initialize queue array
            let q = [],
                isProgress = false;

            // proceed next queue if present
            let next = function (){
                if (q.length > 0) {
                    this.start(q.shift());
                } else {
                    isProgress = false;
                }
            };

            // start sync-utility
            let sync = new Sync(next);

            next.bind(sync);

            // synchronize all requests through queue
            let proceed = ( (sync) => {
                return function (message, locale) {
                    q.push({"message": message, "lang": locale});
                    if (!isProgress) {
                        sync.start(q.shift());
                        isProgress = true;
                    }
                };
            }).call(null, sync);
            socket(proceed);
        }
    }catch (e) {
        console.error("Could not start the server. Error: " + e.message, e.stack);
        process.exit(0);
    }
})();


/*!
 * contentstack-sync
 * copyright (c) Contentstack.com
 * MIT Licensed
 */

'use strict';

/**
 * Module Dependencies.
 */
const path = require('path'),
    _ = require('lodash');
    

let config_instance = null;

module.exports = (_config) => {
    if (_.isPlainObject(_config)) {
        class config {
            constructor (cf){
                try{
                    if (!config_instance) {
                        let _config = cf;

                        let env = _config['environment'];
                        _config['environment'] = process.env.NODE_ENV || env || "development";

                        let server = _config['server'];
                        _config['server'] = process.env.SERVER || process.env.server || server || 'default';

                        let logs = _config['path']['logs'];
                        if(logs){
                            let log_path = path.resolve(logs)
                            _config['path']['logs'] = log_path || process.env.SITE_PATH || process.cwd();
                        }else{
                            _config['path']['logs'] = process.env.SITE_PATH || process.cwd();
                        }

                        this._config = _config;
                        config_instance = this;
                    }
                    return config_instance;
                } catch (err){
                    console.log('Something went wrong while getting config',err);
                }
            }
            get(key){
                return key.split('.').reduce( (o, x) => {
                    if (o && typeof o[x] !== 'undefined') return o[x];
                    return undefined;
                }, this._config);
            }
            set(key, value){
                let config = this._config;
                let list = key.split('.'),
                    sub_key = {},
                    parent_key = {},
                    flag = true;

                if(key == list[0]){
                    config[key] = value
                }
                else{
                    list.map( (val) => {
                        if (_.has(config, val) && flag) {
                            sub_key = config[val];
                            parent_key = config[val];
                            flag = false;
                        } else {
                            if (_.isEmpty(parent_key)) {
                                config[key] = value
                            } else {
                                if (val == list[list.length - 1]) {
                                    sub_key[val] = value;
                                } else {
                                    if (sub_key[val] === undefined) {
                                        sub_key[val] = {};
                                        sub_key = sub_key[val]
                                    } else {
                                        sub_key = sub_key[val]
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
        return new config(_config);
    } else {
        return config_instance;
    }
}



/*!
 * contentstack-sync
 * copyright (c) Contentstack.com
 * MIT Licensed
 */

'use strict';

/*!
 * Module dependencies
 */

const prompt = require('prompt');

let prominent = require('./sync/observable.js'),
    observable = new prominent(),
    utils = require('./utils/index'),
    log = utils.sync,
    config = require('./config')();


exports.url = 'https://contentstack.built.io/';

exports.title = 'Built.io Contentstack';


exports.confirm = (lang, backup, callback) => {
    try {
        let languages = config.get('languages');
        if(languages && lang) {
                        if(backup === undefined) {
                            prompt.message = prompt.delimiter = "";
                            prompt.get([{
                                name: 'confirm',
                                description: 'Do you want to create a backup of the existing content? (Yes/No):',
                                message: 'Please provide confirmation.',
                                required: true,
                                conform: (value) => {
                                    value = value.toLowerCase();
                                    return ( value == "yes" || value == "no" );
                                },
                                before: (value) => {
                                    return value.toLowerCase();
                                }
                            }], (err, result) => {
                                if(err) throw err;
                                let ok = (result.confirm === "yes") ? true : false;
                                process.stdin.destroy();
                                if (ok) {
                                    observable.backup(result.confirm, (data) => {
                                        if(data && data !== undefined){
                                            if (data.status !== -1) {
                                                log.info("Backup Completed Successfully.")
                                                callback();
                                            }
                                        }
                                       else {
                                            log.error("Backup creation failed: ",data)
                                        }
                                    });
                                } else {
                                    callback();
                                }
                            });
                        } else if (backup === false) {
                            callback();
                        } else {
                            observable.backup('yes', (data) => {
                                if(data && data !== undefined){
                                    if (data.status !== -1) {
                                        log.info("Backup Completed Successfully.")
                                        callback();
                                    }
                                }
                                else {
                                    log.error("Backup creation failed: ",data)
                                }
                            });
                        }
            } else {
                throw new Error("Language code is incorrectly specified or is not present in the configuration file.");
            }
    } catch (e) {
        log.error("Error in Confirm : ", e.message);
        callback(e.message);
    }
};

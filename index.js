
/*!
 * contentstack-sync
 * copyright (c) Contentstack.com
 * MIT Licensed
 */

'use strict';

exports.start = (confg) => {
	require('./lib/config')(confg);
    let Observable = require('./lib/sync/observable');
    let Sync = new Observable('sync');
    return Sync;
};

exports.init = (confg) => {
    require('./lib/config')(confg);
    let Observable = require('./lib/sync/observable');
    let Sync = new Observable('initial');
    return Sync;

};


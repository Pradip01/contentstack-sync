const request = require('request');
const assert = require('chai').assert;
const _ = require('lodash');
const _config = { port: 4000,
  cache: true,
  logs: { console: true, json: true, path: './_logs' },
  view: 
   { module: 'nunjucks',
     extension: 'html',
     scaffold: true,
     minify: true },
  languages: 
   [ { code: 'en-us', relative_url_prefix: '/' },
     { code: 'ja-jp', relative_url_prefix: '/jp/' } ],
  storage: { provider: 'FileSystem', options: { basedir: './_content' } },
  assets: 
   { pattern: '/assets/:uid/:filename',
     basedir: './_content',
     options: 
      { dotfiles: 'ignore',
        etag: true,
        extensions: [Object],
        fallthrough: true,
        index: 'index.html',
        lastModified: true,
        maxAge: 0,
        redirect: true,
        setHeaders: [] } },
  static: 
   { url: '/static',
     path: 'public',
     options: 
      { dotfiles: 'ignore',
        etag: true,
        extensions: [Object],
        fallthrough: true,
        index: 'index.html',
        lastModified: true,
        maxAge: 0,
        redirect: true,
        setHeaders: [] } },
  contentstack: 
   { host: 'https://api.contentstack.io',
     version: 'v3',
     socket: 'https://contentstack-realtime.built.io/',
     urls: 
      { stacks: '/stacks/',
        content_types: '/content_types/',
        entries: '/entries/',
        assets: '/assets/',
        environments: '/environments/',
        publish_queue: '/publish-queue/',
        session: '/user-session/',
        user: '/user/' },
     events: { delete: 'delete', publish: 'publish', unpublish: 'unpublish' },
     types: { asset: 'asset', entry: 'entry', form: 'form' },
     api_key: 'blt879409b3e30a8004',
     access_token: 'blt9920d5bcf50e54d7' },
  path: {},
  environment: 'development',
  server: 'default',
  provider: '',
  type: '',
  content_types: '',
  skip_content_types: '',
	storagePath: ''
 }


let config = require('../lib/config')(_config);
const inputs = {
    ctUID: 'categories',
    entryUID: 'bltfafd9f899588d986',
    version: 1,
    remove: false,
    eventText: '',
    lang: 'en-us'
};

const demo_entry = {
  "title": "Fruits",
  "url": "/fruits",
  "tags": [],
  "locale": "en-us",
  "uid": "bltfafd9f899588d986",
  "created_by": "bltd94c186015b52323d9429c90",
  "updated_by": "bltd94c186015b52323d9429c90",
  "created_at": "2018-01-30T19:24:57.633Z",
  "updated_at": "2018-01-30T19:24:57.633Z",
  "_version": 1
}

function next () {
  if (q.length > 0) {
    let entryData = q.shift();
    this.sync.start(entryData);
    log.info("\nEntry " + JSON.stringify(entryData.message.body));
  } else {
    isProgress = false;
    log.info("Synchronization requests completed successfully");
  }
};

let sync = null;
const Sync = require('../lib/sync/sync');

    let api = config.get('contentstack');
    let headers = {
        api_key: config.get('contentstack.api_key'),
        access_token: config.get('contentstack.access_token')
    }
    let environment = config.get('environment');
// Note: Do not use arrow functions for mocha
describe.only('Test entry publish', function () {
	it ('Test import of sync', function (done) {
		sync = new Sync(next, true);
		return done();
	});
	it('Get Environment', function(){
		if(environment) {
				request({
						url: api.host + api.urls.environments + "development",
						method: "GET",
						headers: headers,
						json: true
				}, function(err, res, body) {
						if(!err && body && body.environment) {
								environment = body.environment;
								assert.ok("Environment found");
						} else {
								assert("Environment not found error."+(err && err.message));
						}
				});
		} else {
				assert.ok("Environment not exists");
		}
})
	it ('Check if the sync has the mandatory methods', function () {
		let mandatory_methods = [
      "start",
      "next",
      "entry",
      "asset",
			"form",
      "bulkAssetDelete"
    ];
    let syncMethods = Object.getOwnPropertyNames(
      Object.getPrototypeOf(sync)
		);
    _.map(mandatory_methods, method => {
      assert.isOk(
        syncMethods.indexOf(method),
        method + " present in sync?"
      );
      assert.isFunction(sync[method], method + " type is function?");
    });
	});
	it ('Test request call being made for published entry', function (done) {

		return done();
	})
});